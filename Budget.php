<?php

/**
 * Modelagem dos dados de um orçamento.
 *
 * @author Pablo R. Quadrado <dev.pabloquadrado@gmail.com>
 */
class Budget {

    /** @var float Preço do orçamento. */
	private $price;

    /**
     * Construtor de um orçamento.
     *
     * Define a propriedade {@see Budget::$price}
     *
     * @param float $newPrice
     */
	public function __construct($newPrice)
    {
        $this->price = $newPrice;
	}

    /**
     * Retorna a propriedade {@see Budget::$price}
     *
     * @return float
     */
	public function getPrice()
	{
		return $this->price;
	}
}