<?php

/**
 * Imposto de ICMS.
 *
 * @author Pablo R. Quadrado <dev.pabloquadrado@gmail.com>
 */
class ICMS extends TaxDecorator {

    /**
     * @inheritDoc
     */
    protected function useMaximumTax(Budget $budget)
    {
        return $budget->getPrice() > 300;
    }

    /**
     * @inheritDoc
     */
    protected function maximumTax(Budget $budget)
    {
        return $budget->getPrice() * 0.5;
    }

    /**
     * @inheritDoc
     */
    protected function minimumTax(Budget $budget)
    {
        return $budget->getPrice() * 0.1;
    }
}
