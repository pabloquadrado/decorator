<?php

require_once 'loadFiles.php';

$budget = new Budget(400);

$taxCalculator = new TaxCalculator();

$taxAmountOfICMS = $taxCalculator->getTaxesAmount($budget, new ICMS(new ISS()));
$taxAmountOfISS = $taxCalculator->getTaxesAmount($budget, new ISS());

echo 'Imposto de ICMS sobre R$' . $budget->getPrice() . ': R$'. $taxAmountOfICMS . '<br><br>';
echo 'Imposto de ISS sobre R$' . $budget->getPrice() . ': R$'. $taxAmountOfISS . '<br><br>';
